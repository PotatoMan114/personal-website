from django.apps import AppConfig


class DinnerdeciderConfig(AppConfig):
    name = 'dinnerDecider'
