from django.db import models

# Create your models here.

class Recipe(models.Model):
    type = models.CharField(max_length=25)
    name = models.CharField(max_length=200)
    complexity = models.IntegerField(default=1)
    price = models.FloatField(default=1.0);
    # ingredients through foreign key relationship (one-tom-many)
    

class Ingredient(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    price = models.FloatField(default=1.0);
