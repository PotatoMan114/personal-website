const app = {

    data() {
        return {
            resultRetrieved : false,
            results : [
                { raw : "test1", reverse : "1tset", palendrome : "false"},
                { raw : "test1tset", reverse : "test1tset", palendrome : "true"},
            ],
        }
    },
    delimiters: ['[[', ']]'],
    
    methods: {
        checkPalendrome(event) {
            console.log("checking");
            var input = document.querySelector('#palendromeInput').value;
            console.log(`input: ${input}`);
            fetch(`http://${window.location.host}/reverseAPI/palendrome?raw=${input}`)
                .then(r => r.json())
                .then(json => {
                    if (json.error) {
                        console.log(json.error);
                        this.results.push({error : "true", errorMessage : json.error});
                    }
                    else {
                        this.resultRetrieved = true;
                        this.results.push({raw : input, reverse : json.reverse, palendrome : json.palendrome});
                    }
                });
        },
    },
};

Vue.createApp(app).mount('#app');
