from django.urls import path

from . import views

app_name = 'unitconv'

urlpatterns = [
    path('palendrome/', views.palendrome, name='palendrome'),
    path('reverse/', views.reverse, name='reverse'),
]
