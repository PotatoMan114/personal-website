from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.


def reverse(request):

#comes in this form:
# http://localhost:8000/reverseAPI/reverse?raw=_____

#returns data in this form:
# {"reverse" : "______"}
    response = {}

    if 'raw' not in request.GET:
        response['error'] = "Error!! Nothing recieved to reverse"
    if 'raw'.length == 0:
        response['error'] = "Error!! Nothing recieved to reverse"
    else:
        raw = request.GET['raw']
        reverse = raw[::-1]
        response['reverse'] = reverse
    response = JsonResponse(response)
    response['Access-Control-Allow-Origin'] = '*'
    return response


def palendrome(request):

#comes in this form:
# http://localhost:8000/reverseAPI/palendrome?raw=_____

#returns data in this form:
# {"reverse" : "______", "palendrome" : "true" or "false"}

    response = {}

    if 'raw' not in request.GET:
        response['error'] = "Error!! Nothing recieved to reverse"
    else:
        raw = request.GET['raw']
        reverse = raw[::-1]
        response['reverse'] = reverse
        if raw.replace(" ", "") == reverse.replace(" ", ""):
            response['palendrome'] = "true"
        else:
            response['palendrome'] = "false"
    response = JsonResponse(response)
    response['Access-Control-Allow-Origin'] = '*'
    return response
